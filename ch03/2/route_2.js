/**
 * Created by 杨涛 on 2020/4/3.
 */
function route(handle, pathname) {
    console.log("About to route a request for " + pathname);
    if (typeof handle[pathname] === 'function') {
        handle[pathname]();
    } else {
        console.log("没有对应的请求处理方式 " + pathname);
    }
}

exports.route = route;