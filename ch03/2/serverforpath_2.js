/**
 * Created by 杨涛 on 2020/4/3.
*/
var http = require("http");
var url = require("url");

function start(route, handle) {
    
    //定义了请求事件的处理方式
    function onRequest(request, response) {
        var pathname = url.parse(request.url).pathname;
        console.log("Request for " + pathname + " received.");

        route(handle, pathname);

        response.writeHead(200, {"Content-Type": "text/plain"});
        response.write("Hello World");
        response.end();
    }

    //启动服务，监听8888端口，收到消息，交给onRequest方法处理
    http.createServer(onRequest).listen(8888);
    console.log("服务启动了");
}
//外部如果 要访问本文件，这个方法叫start
exports.start = start;