/**
 * Created by 杨涛 on 2020/4/3.
 */
var server = require("./serverforpath_2");
var router = require("./route_2");
var requestHandlers = require("./requestHandlers");

var handle = {}
//给一个路径赋值，什么值？（方法）
handle["/"] = requestHandlers.start;
handle["/start"] = requestHandlers.start;
handle["/upload"] = requestHandlers.upload;

server.start(router.route, handle);