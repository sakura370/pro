/**
 * Created by 杨涛 on 2020/4/3.
 */
function route(handle, pathname, response) {
    console.log("请求的路径是： " + pathname);
    if (typeof handle[pathname] === 'function') {
        handle[pathname](response);
    } else {
        console.log("No request handler found for " + pathname);
        response.writeHead(404, {"Content-Type": "text/plain"});
        response.write("404 Not found");
        response.end();
    }
}

exports.route = route;