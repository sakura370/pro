/**
 * Created by 杨涛 on 2020/4/3.
 */
function route(handle, pathname, response, postData) {
    console.log("About to route a request for " + pathname);
    if (typeof handle[pathname] === 'function') {
        handle[pathname](response, postData);
    } else {
        console.log("没有找到路径。。。" + pathname);
        response.writeHead(404, {"Content-Type": "text/plain"});
        response.write("404 Not found，找到不资源 ");
        response.end();
    }
}

exports.route = route;