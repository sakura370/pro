/**
 * Created by 杨涛 on 2020/4/3.
 */
function start(response, postData) {
    console.log("开始的方法 启动了.");

    var body = '<html>'+
        '<head>'+
        '<meta http-equiv="Content-Type" content="text/html; '+
        'charset=UTF-8" />'+
        '</head>'+
        '<body>'+
        '<form action="/upload" method="post">'+
        '<textarea name="text" rows="20" cols="60"></textarea>'+
        '<input type="submit" value="提交" />'+
        '</form>'+
        '</body>'+
        '</html>';
    

    response.writeHead(200, {"Content-Type": "text/html"});
    response.write(body);
    response.end();
}

function upload(response, postData) {
    console.log("上传的方法开始执行.");
    response.writeHead(200, {"Content-Type": "text/plain"});
    response.write("You've sent: " + postData);
    response.end();
}

exports.start = start;
exports.upload = upload;